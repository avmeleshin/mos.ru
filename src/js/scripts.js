$(document).ready(function() {
	/* Фокус в инпут */
	$('.inputBlock input').focus(onInput);

	/* Нажатие на лейбел */
	$('.inputBlock label').on('click', function(){
		onInput();
		$(this).siblings('input').focus();
	});
	
	/* Потеря фокуса инпута */
	$('.inputBlock input').focusout(function() {
		var l = $(this).val().length;
		if (l == 0) {
			var lb = $(this).parent().find('label');
			lb.stop().animate({
				fontSize : "0.800em",
				top      : 19

			}, 250)			
		}
	});

	/* Маска ввода телефона */
	$('input[name="tel"]').focus(function(){
		$(this).mask("+7 (999) 999-99-99");
	});

	/* Открытый закрытый глаз */
	$('.eye').removeClass('open');

	$('.eye').on('click', function() {
		var pass = $('#password');
		if (pass.val().length > 0) {
			if ($(this).hasClass('open')) {
				$(this).removeClass('open');
				pass.attr('type', 'password');
			} else {
				$(this).addClass('open');
				pass.attr('type', 'text');			
			};			
		}
	});

	/* Селекс выбора города */
	$('#city').selectmenu({
		change : function() {
			$('.ui-selectmenu-text').css('color', '#171b20');
			$('.ui-selectmenu-text').css('font-family', '"RobotoMedium", sans-serif');
		}
	});

	/* Выбор пола */
	$('.swithcField').on('click', function() {
		$('input[name="sex"]').change();
	});

});

function onInput() {
	var lb = $(this).parent().find('label');
	lb.stop().animate({
		fontSize : "0.700em",
		top      : 9
	}, 250)
};