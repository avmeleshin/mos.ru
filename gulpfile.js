'use strict'

var gulp      = require('gulp'),				// Подключаем Gulp
	postcss   = require('gulp-postcss'),
	aprefixer = require('autoprefixer'),
	cssnano   = require('cssnano'),
	uglify    = require('gulp-uglify'),
	rigger    = require('gulp-rigger'),
	cache     = require('gulp-cache'),
	imagemin  = require('gulp-imagemin'),
	pngquant  = require('imagemin-pngquant'),
	sass      = require('gulp-sass'),
	bourbon   = require('node-bourbon'),
	pug	      = require('gulp-pug'),
	rimraf    = require('rimraf'),
	rename    = require('gulp-rename'),
	brwsrSync = require('browser-sync'),
	reload    = brwsrSync.reload;

// Пути
var path = {
	build : {							// Готовые файлы проекта
		html  : 'dist/',
		js    : 'dist/js/',
		style : 'dist/css/',
		img   : 'dist/img/',
		fonts : 'dist/fonts/'
	},
	src   : {							// Исходники проекта
		html  : 'src/pug/**/*.pug',
		js    : 'src/js/**/*.js',
		scss  : 'src/sass/**/*.+(sass|scss)',
		img   : 'src/img/**/*.*',
		fonts : 'src/fonts/**/*.*'
	},
	watch : {							// За какими файлами наблюдать
		html  : 'src/pug/**/*.pug',
		js    : 'src/js/**/*.js',
		scss  : 'src/sass/**/*.+(sass|scss)',
		img   : 'src/img/**/*.*',
		fonts : 'src/fonts/**/*.*'
	},
	clean : './dist'
};

// Настройки сервера
var config = {
	server : {
		baseDir: "./dist"
	},
	//tunnel : true,
	host   : 'localhost',
	port   : 8013,
	logPrefix : "alvm13"
}

var processors = [
		aprefixer({
			browsers: [
				'last 15 versions',
				'>1%',
				'ie 8'
			],
			cascade: false
		}),
		cssnano({
			convertValues: {
				length: false
			},
			discardComments: {
				removeAll: true
			},
			discardUnused: false
	})
];

gulp.task('html:build', function(){
	gulp.src(path.src.html)
		.pipe(pug({
			pretty: true
		}))
		.pipe(gulp.dest(path.build.html))
		.pipe(reload({stream: true}));
});

// Сборка скриптов проекта
gulp.task('js:build', function(){
	return gulp.src(path.src.js)
		.pipe(rigger())
		.pipe(uglify())
		.pipe(rename({suffix: '.min'}))
		.pipe(gulp.dest(path.build.js))
		.pipe(reload({stream: true}));
});

// Сборка стилей
gulp.task('scss:build', function(){
	return gulp.src(path.src.scss)
		.pipe(sass({
			includePaths: bourbon.includePaths
		}))
		.pipe(postcss(processors))
		.pipe(rename({suffix: '.min'}))
		.pipe(gulp.dest(path.build.style))
		.pipe(reload({stream: true}));
});

// Обработка изображений
gulp.task('img:build', function(){
	return gulp.src(path.src.img)
		.pipe(cache(imagemin({
			optimizationLevel: 5,
			interlaced  : true,
			progressive : true,
			use: [pngquant()]
		})))
		.pipe(gulp.dest(path.build.img))
		.pipe(reload({stream: true}));
});

// Копируем шрифты
gulp.task('fonts:build', function(){
	gulp.src(path.src.fonts)
		.pipe(gulp.dest(path.build.fonts))
		.pipe(reload({stream: true}));
});

// Запуск локального сервера
gulp.task('browser-sync', function(){
	brwsrSync(config);
});

// Очистка кеша
gulp.task('cc', function (done) {
	return cache.clearAll(done);
});

// Сборка всего проекта
gulp.task('build', [
	'html:build',
	'js:build',
	'scss:build',
	'fonts:build',
	'img:build'
]);

// Автоматический запуск тасков
gulp.task('watch', function () {
	gulp.watch([path.watch.scss], function(event, cb) {
		gulp.start('scss:build');
	});
	gulp.watch([path.watch.js], function(event, cb) {
		gulp.start('js:build');
	});
	gulp.watch([path.watch.img], function(event, cb) {
		gulp.start('img:build');
	});
	gulp.watch([path.watch.fonts], function(event, cb) {
		gulp.start('fonts:build');
	});
	gulp.watch([path.watch.html], function(event, cb) {
		gulp.start('html:build');
	});
});

// Запуск локального сервера
gulp.task('browser-sync', function(){
	brwsrSync(config);
});

// Очитка проекта
gulp.task('clean', function(cb){
	rimraf(path.clean, cb);
});

gulp.task('default', ['build', 'browser-sync', 'watch']);